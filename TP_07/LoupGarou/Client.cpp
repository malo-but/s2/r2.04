#include <Arduino.h>
#include <WiFi.h>

#define LGFX_WT32_SC01 
#define LGFX_AUTODETECT // Autodetect board
#define LGFX_USE_V1     // set to use new version of library
#include <LovyanGFX.hpp> // main library
#include <lvgl.h>
#include "lv_conf.h"
LV_FONT_DECLARE (lib_sans_12);

#include "R204Map.h"
#include "SetupNet.h"
#include "Client.h"

static const int DELAY_TRY = 200;

//  =======================================
static char * labelsConnection [] = {
  "#ff0000 Déconnecté·e·s#",
  "#00ff00 Connecté·e·s#"
};

//  =======================================
static enum ClientState {
  ID_UNKNOWED,
  ID_WAIT,
  ID_SET,
  ROLE_DEFINED
} clientState (ID_UNKNOWED);

static String pseudo ("");

bool connected (false);

//  =======================================
static void
serverConnection (const String &methode, const String &req,
		  int &code, R204Map<String, String> &headers, String &rep) {
  code = 0;
  headers.clear ();
  rep = "";

  connected = false;
  for (int i (0); i < 3; ++i) {
    if (WiFi.status () == WL_CONNECTED) {
      connected = true;
      break;
    }
    WiFi.begin (getSSID ().c_str (), password);
    delay (DELAY_TRY);
  }
  WiFiClient client;
  if (!client.connect (webHost, webPort))
    return;
  client.println (methode+" /" + req + " HTTP/1.1\r\n"+
		  "Host: " + webHost + "\r\n" +
		  "Connection: close\r\n"+
		  "\r\n");
  client.flush ();
  String line, last;
  while (client.connected ()) {
    if (! client.available ())
      continue;
    char c = client.read ();
    if (c == '\r')
      // ignore MS
      continue;
    if (c != '\n') {
      // traiter ligne entière
      line += c;
      continue;
    }
    if (!line.length ())
      // fin des entêtes
      break;
    if (!code) {
      // première ligne = "HTTP/1.1 code text"
      int a (line.indexOf (' '));
      int b (line.indexOf (' ', a+1));
      if (a < 0 || b < 0)
	Serial.println ("\n   *** Bug HTTP");
      code = line.substring (a+1, b).toInt ();
    } else if (line[0] == ' ' || line[1]== '\t') {
      // valeur sur plusieurs lignes
      if (last.length ())
	last += line.substring (1);
      else
	Serial.println ("\n   *** Bug MIME +");
    } else {
      // variable d'entête
      int a (line.indexOf (':'));
      if (a < 1)
	Serial.println ("\n   *** Bug MIME :");
      String key (line.substring (0, a));
      key.toLowerCase ();
      headers [key] = line.substring (a+1);
    }
    // ligne suivante
    line = "";
  }
  // lire les données
  int contentLenght (headers ["content-length"].toInt ());
  char result [contentLenght+1];
  for (int i = 0; i < contentLenght; ++i)
    result [i] = client.read ();
  result [contentLenght] = '\0';
  rep = String (result);
  WiFi.mode (WIFI_OFF);
  Serial.println (String (code)+": "+rep);
}

//  =======================================
void sendPseudo (String pseudo) {
  int code;
  R204Map<String, String> headers;
  String rep;
  serverConnection ("GET", String ("pseudo?pseudo=")+pseudo, code, headers, rep);
  delay (3000);
}

//  =======================================
static void updateStatus () {
  static lv_obj_t *statusBar (NULL);
  String msg (getLocalMac ()+" "+getSSID ()+":"+labelsConnection [connected ? 1 : 0]+" >"+pseudo);
  if (!statusBar) {
    statusBar = lv_label_create (lv_scr_act ());
    lv_obj_set_style_text_font (statusBar, &lib_sans_12, 0);
    lv_label_set_recolor (statusBar, true);
    lv_label_set_text (statusBar, msg.c_str ());
    lv_obj_set_size (statusBar, 480, 20);
    lv_obj_align (statusBar, LV_ALIGN_TOP_MID, 0, 0);
    return;
  }
  if (lv_label_get_text (statusBar) != msg.c_str ())
    lv_label_set_text (statusBar, msg.c_str ());
}

static void updatePseudo(lv_event_t *e) {

  lv_event_code_t code = lv_event_get_code(e);
  lv_obj_t *kb = lv_event_get_target(e);
  lv_obj_t *ta = (lv_obj_t *) lv_event_get_user_data(e);
  if(code != LV_EVENT_READY)
    return;
  pseudo = String(lv_textarea_get_text(ta));

  clientState = ID_SET;

} 

static lv_obj_t *kb (nullptr);

void displayKB(bool show) {
    if (!show) {
      if (kb)
        lv_obj_del (kb);
      kb = nullptr;
      return;
  }
  if (kb)
    return;
  /*Create an AZERTY keyboard map*/
  static const char *kb_map[] = {
    "A", "Z", "E", "R", "T", "Y", "U", "I", "O", "P", LV_SYMBOL_BACKSPACE, "\n",
    "Q", "S", "D", "F", "G", "J", "K", "L", "M",  LV_SYMBOL_NEW_LINE, "\n",
    "W", "X", "C", "V", "B", "N", ",", ".", ":", "!", "?", "\n",
    LV_SYMBOL_CLOSE, " ",  " ", " ", LV_SYMBOL_OK, NULL
  };

  /*Set the relative width of the buttons and other controls*/
  static const lv_btnmatrix_ctrl_t kb_ctrl[] = {
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 6,
    4, 4, 4, 4, 4, 4, 4, 4, 4, 6,
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
    2, LV_BTNMATRIX_CTRL_HIDDEN | 2, 6, LV_BTNMATRIX_CTRL_HIDDEN | 2, 2
  };

  /*Create a keyboard and add the new map as USER_1 mode*/
  lv_obj_t *kb = lv_keyboard_create (lv_scr_act ());

  lv_keyboard_set_map (kb, LV_KEYBOARD_MODE_USER_1, kb_map, kb_ctrl);
  lv_keyboard_set_mode (kb, LV_KEYBOARD_MODE_USER_1);

  /*Create a text area. The keyboard will write here*/
  lv_obj_t *ta;
  ta = lv_textarea_create (lv_scr_act ());
  lv_obj_align (ta, LV_ALIGN_TOP_MID, 0, 10);
  lv_obj_set_size (ta, lv_pct (90), 80);
  lv_obj_add_state (ta, LV_STATE_FOCUSED);

  lv_keyboard_set_textarea (kb, ta);
  lv_obj_add_event_cb (kb, updatePseudo, LV_EVENT_READY, ta);
}


//  =======================================
void clientLoop () {
  switch (clientState) {
  case ID_UNKNOWED:
    // XXX affiche un clavier si necassaire pour saisir un pseudo
    // XXX la validation envoie le pseudo

    displayKB(true);

    break;
  case ID_WAIT:
    // XXX si le pseudo est incorrecte revenir à la saisie

    displayKB(true);

    break;
  case ID_SET:
    // XXX si le pseudo est bon attendre le rôle

    displayKB(false);

    break;
  case ROLE_DEFINED:
    // XXX attendre les actions de vote

    displayKB(false);

    break;
  }
  updateStatus ();
  sendPseudo (pseudo);
}

//  =======================================
